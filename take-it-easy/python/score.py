"""TODO: docstring"""

# TODO: rename `confidence` to `margin`?


import logging
import operator
import sys
from typing import List, Optional, Tuple

import cv2
import numpy
import os
from os import path

from time_this import time_this


logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

# Find the directory containing this script (see [1]) and set up variables for some
# directory paths relative to it which we need.
#
# [1]: https://stackoverflow.com/q/595305
#      "How do I get the path of a the Python script I am running in?"
_base_dir   = path.join(path.dirname(__file__), '..')
_digits_dir = path.join(_base_dir, 'digits')

_board_template = cv2.imread(path.join(_base_dir, 'board_template.png'))
# I also tried kernel sizes of (13, 13) and (9, 9).  Both gave worse results *on average*.
_board_template = cv2.GaussianBlur(_board_template, (7, 7), 0, 0)


def rotate_without_cropping(image, degrees):
    """See [1] and [2].

    [1]: https://www.pyimagesearch.com/2017/01/02/rotate-images-correctly-with-opencv-and-python/
    [2]: https://docs.opencv.org/master/da/d54/group__imgproc__transform.html#gafbbc470ce83812914a70abfb604f4326
    """
    m, n = image.shape[:2]  # `m` is the number of rows, `n` the number of columns
    center = center_x, center_y = n // 2, m // 2
    rot_mat = cv2.getRotationMatrix2D(center=center, angle=degrees, scale=1)
    cos = numpy.abs(rot_mat[0, 0])
    sin = numpy.abs(rot_mat[0, 1])
    new_n, new_m = map(numpy.ceil, (m * sin + n * cos, m * cos + n * sin))
    rot_mat[0, 2] += (new_n / 2) - center_x
    rot_mat[1, 2] += (new_m / 2) - center_y
    return cv2.warpAffine(image, rot_mat, (int(new_n), int(new_m)))


# TODO: since we erode the image that is searched with template matching, also erode the
# digits?  TODO: throw away two of the channels?
def _load_digit_images(_digits_dir) -> Tuple[numpy.ndarray]:
    # There is no `0.png`, but OpenCV doesn't care and neither do I.  I just want
    # _load_digit_images[1] to be "1.png".
    digits = tuple(cv2.imread(path.join(_digits_dir, f'{i}.png'), cv2.IMREAD_UNCHANGED)
                   for i in range(10))
    for digit in digits[1:]:
        digit[:, :, 0] = cv2.GaussianBlur(digit[:, :, 0], (3, 3), 0)
    return digits


def get_digit_template(i: int, angle: float=.0) -> Tuple[numpy.ndarray, numpy.ndarray]:
    """The value of the angle is interpreted as radians.  Positive values mean
    counter-clockwise rotation."""
    digits = get_digit_template._digits
    angle = numpy.degrees(angle)
    # Don't rotate the template image if the angle is tiny.  Always rotating it worsens
    # results slightly.
    if abs(angle) <= 1:
        return digits[i][:, :, 0], digits[i][:, :, 3]
    rotated_digit = rotate_without_cropping(digits[i], angle)
    gray  = rotated_digit[:, :, 0]
    alpha = rotated_digit[:, :, 3]
    assert (gray.dtype  == numpy.dtype('uint8'))
    assert (alpha.dtype == numpy.dtype('uint8'))
    return gray, alpha

get_digit_template._digits = _load_digit_images(_digits_dir)


class ObjectFinder():
    def __init__(self, query_image):
        self.query_image = query_image
        with time_this('finding features with SURF', logger.debug):
            self.surf = cv2.xfeatures2d.SURF_create(hessianThreshold=150)
            self.kp1, self.des1 = self.surf.detectAndCompute(query_image, None)
        # print(len(kp1))
        # keypoint_image = cv2.drawKeypoints(query_image, kp1, None, (255, 0, 0), 4)
        # cv2.namedWindow('Keypoints', cv2.WINDOW_NORMAL)
        # cv2.imshow('Keypoints', keypoint_image)
        # while cv2.waitKey() != -1:
        #     pass
        # exit()

    # See <https://docs.opencv.org/trunk/d1/de0/tutorial_py_feature_homography.html>.
    # This whole function is pretty much just copied from there.  FIXME: we crash when
    # there are no matching features at all
    def find_in(self, photo, show=False):
        # Converting the photo to grayscale (`cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)`)
        # doesn't make things faster.
        with time_this('finding features with SURF', logger.debug):
            kp2, des2 = self.surf.detectAndCompute(photo, None)

        with time_this('finding perspective transformation', logger.debug):
            FLANN_INDEX_KDTREE = 1
            index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
            search_params = dict(checks=50)
            flann = cv2.FlannBasedMatcher(index_params, search_params)
            matches = flann.knnMatch(self.des1, des2, k=2)

            # Store all the good matches using Lowe's ratio test.
            good = []
            for m, n in matches:
                if m.distance < 0.7 * n.distance:
                    good.append(m)

            src_pts = numpy.float32([self.kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = numpy.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

        if show:
            matchesMask = mask.ravel().tolist()

            h, w, d = self.query_image.shape
            pts = numpy.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(
                -1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)

            labeled_photo = photo.copy()
            cv2.polylines(labeled_photo, [numpy.int32(dst)], True, color=(0, 0, 0),
                        thickness=8, lineType=cv2.LINE_AA)

            draw_params = dict(
                matchColor=(0, 255, 0),  # Draw matches in green.
                singlePointColor=None,
                matchesMask=matchesMask,  # Only draw inliers.
                flags=2)
            result_image = cv2.drawMatches(
                self.query_image, self.kp1, labeled_photo, kp2, good, None, **draw_params)
            cv2.namedWindow('Matches', cv2.WINDOW_NORMAL)
            cv2.imshow('Matches', result_image)

        return M


def locate_board(photo_path, show=False):
    """Locate the board in a photograph of a completed game of Take It Easy."""
    cache_dir = path.join(path.dirname(__file__), 'cache')
    os.makedirs(cache_dir, exist_ok=True)
    board_path = path.join(cache_dir, path.basename(photo_path))

    # Check if we already saved the transformed and cropped board to disk.
    if path.isfile(board_path):
        board = cv2.imread(board_path)
    else:
        photo = cv2.imread(photo_path)
        M = locate_board.finder.find_in(photo, show=True)
        board_height, board_width = _board_template.shape[:2]
        board = cv2.warpPerspective(photo, M, (board_width, board_height),
                                    flags=cv2.WARP_INVERSE_MAP)
        cv2.imwrite(board_path, board)
    return board

locate_board.finder = ObjectFinder(_board_template)


# See <https://www.pyimagesearch.com/2014/05/26/opencv-python-k-means-color-clustering>
# and <http://scikit-learn.org/stable/auto_examples/cluster/plot_color_quantization.html>.
def quantize_colors(board):
    from sklearn.cluster import KMeans
    from sklearn.utils import shuffle

    w, h, _ = board.shape

    # Reshape the image to be an array of color triples.
    color_array = board.reshape((w * h, 3))

    # Fit with a subset of the pixels, else this takes about 10 minutes.
    color_array_sample = shuffle(color_array, random_state=0)[:10000]
    # We know there should be about 12 different colors.  We have some idea what they are,
    # but it's pretty rough because we don't know the lightning conditions or anything
    # about the camera used to take the photo.  As long as none of the three colors that
    # are possible for each of the three directions are identical after quantization, we
    # are fine.  If two are identical, we can probably at least detect that.  If we get
    # four colors for one direction, that could be bad.  XXX: I'm using one extra cluster
    # for the background around the tiles.  With twelve, the "1" and "4" lines got the
    # same color.  I hope I can crop all background and go back to using 12 colors,
    # though.
    twelve_means = KMeans(n_clusters=13).fit(color_array_sample)
    labels = twelve_means.predict(color_array)

    quantized = numpy.zeros_like(board)
    label_idx = 0
    for i in range(w):
        for j in range(h):
            quantized[i][j] = twelve_means.cluster_centers_[labels[label_idx]]
            label_idx += 1
    return quantized


class Numeral():
    """Dumb class that represents a symbol which is printed on a tile in an image of a
    game board.  The symbol stands for a digit that we want to identify.  The class
    doesn't know much by itself.  You can get or set the size of the ROI where the digit
    is expected to be printed.  An object may contain the actual digit that was
    identified, its location, and the confidence of the identification.
    """

    # The default ROI dimensions.  These class variables may be overridden with instance
    # variables to amend the ROI height and width.
    roi_height = 170
    roi_width  = 150

    candidate_triples = (1, 5, 9), (2, 6, 7), (3, 4, 8)

    # TODO: store `top_left` and `bottom_right` in absolute coordinates so they don't need
    # to be adjusted when the tile's angle or offset is changed?  How can we get the
    # numeral's offset from it's expected position, though?  I guess we need the tile for
    # that anyway, since we don't know about the tile's rotation here.
    def __init__(self):
        self.digit:        Optional[int] = None
        self.top_left:     Optional[Tuple[int, int]] = (None, None)
        self.bottom_right: Optional[Tuple[int, int]] = (None, None)
        self.confidence:   Optional[float] = None

    def good(self) -> bool:
        """Check whether the numeral was identified with good confidence."""
        return self.confidence >= self._good_confidence

    def bad(self) -> bool:
        """Check whether the numeral was identified with poor confidence."""
        return not self.good()

    def get_center(self) -> Tuple[float, float]:
        """Get the numeral's center relative to it's ROI."""
        return (self.top_left[0] + self.bottom_right[0]) / 2, \
               (self.top_left[1] + self.bottom_right[1]) / 2

    # def get_offset(self) -> Tuple[float, float]:
    #     """Get the numeral's deviation from being perfectly centered in its ROI."""
    #     numeral_center = self.get_center()
    #     ideal_center = self.roi_height / 2, self.roi_width / 2
    #     return numeral_center[0] - ideal_center[0], numeral_center[1] - ideal_center[1]

    # TODO: dynamically select which digits are used to amend the ROI based on what's
    # available.  If there are two that are slightly above 1.3, use both.  If there's one
    # that's only slightly above 1.3 and another one around 2, only use the latter.
    _good_confidence = 1.3


class Tile():
    """Representation of one of the 19 hexagonal tiles present on the game board of a
    completed round of Take It Easy.  A Tile has three Numeral objects and a configuration
    or pose (location plus orientation).
    """

    angle: float = .0
    offset: Tuple[int, int] = (0, 0)

    def __init__(self):
        # The numerals are at the top, left, and right, respectively.  TODO:
        # <https://docs.python.org/3/library/enum.html>?
        self.numerals = Numeral(), Numeral(), Numeral()

    # TODO: rename this.
    def get_numeral_roi_offset(self, numeral_index: int) -> Tuple[float, float]:
        """Get a numeral's deviation from being perfectly centered in its ROI."""
        # return self.numerals[numeral_index].get_offset()
        numeral = self.numerals[numeral_index]
        # The numeral's center is relative to the tile's default location.
        numeral_center = numeral.get_center()
        ideal_center = tuple(map(operator.add, self.get_expected_offset(numeral_index),
                                 self.offset))
        return numeral_center[0] - ideal_center[0], numeral_center[1] - ideal_center[1]

    def get_numeral_offset(self, numeral_index: int) -> Tuple[float, float]:
        """Get a numeral's actual offset from the tile's center.  (This uses the location
        that was determined when identifying the numeral.)
        """
        default_offset = self.default_numeral_offsets[numeral_index]
        # extra_offset = self.numerals[numeral_index].get_offset()
        extra_offset = self.get_numeral_roi_offset(numeral_index)
        return default_offset[0] + extra_offset[0], default_offset[1] + extra_offset[1]

    # XXX: this doesn't use the tile's offset.
    def get_expected_offset(self, numeral_index: int) -> Tuple[float, float]:
        """Get the offset we expect a numeral to be at based on the tile's rotation."""
        default_offset = self.default_numeral_offsets[numeral_index]
        offset_vector = numpy.array([[default_offset[0]], [default_offset[1]]])
        sin = numpy.sin(self.angle)
        cos = numpy.cos(self.angle)
        # Matrix that performs a counterclockwise direction of a (row, column) vector in a
        # coordinate system with the origin at the top-left.  (This happens to be the same
        # rotation matrix as the one for (x, y) vectors in a standard Cartesian coordinate
        # system.)
        rotation = numpy.matrix([[cos, -sin],
                                 [sin,  cos]])
        expected_offset = (rotation * offset_vector).A1
        return expected_offset[0], expected_offset[1]

    def determine_angle(self, i, j) -> float:
        """Determine the tile's in-plane rotation in radians based on the offsets at which
        numerals i and j were located.
        """
        # The tile's in-plane rotation is the angle between the vector connecting the two
        # digit's positions and the vector connecting their expected positions.
        # numerals = self.numerals[i], self.numerals[j]
        ideal_direction_vector = (
            self.default_numeral_offsets[i][0] - self.default_numeral_offsets[j][0],
            self.default_numeral_offsets[i][1] - self.default_numeral_offsets[j][1])
        positions = self.get_numeral_offset(i), self.get_numeral_offset(j)
        actual_direction_vector = (
            positions[0][0] - positions[1][0],
            positions[0][1] - positions[1][1])

        # See [1] and [2].
        dot = ideal_direction_vector[0] * actual_direction_vector[0] + \
              ideal_direction_vector[1] * actual_direction_vector[1]
        det = ideal_direction_vector[0] * actual_direction_vector[1] - \
              ideal_direction_vector[1] * actual_direction_vector[0]
        angle = numpy.arctan2(det, dot)
        return angle
    # [1]: https://math.stackexchange.com/q/878785
    #         "How to find an angle in range(0, 360) between 2 vectors?"
    # [2]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.arctan2.html

    # def set_offset(self, new_offset):
    #     offset_change = new_offset[0] - self.offset[0], new_offset[1] - self.offset[1]
    #     for numeral in self.numerals:
    #         # Adjust the `top_left` and `bottom_right` members of the numerals.
    #         numeral.top_left = (numeral.top_left[0] - offset_change[0],
    #                             numeral.top_left[1] - offset_change[1])
    #         numeral.bottom_right = (numeral.bottom_right[0] - offset_change[0],
    #                                 numeral.bottom_right[1] - offset_change[1])
    #     self.offset = new_offset

    def add_offset(self, extra_offset):
        self.offset = self.offset[0] + extra_offset[0], self.offset[1] + extra_offset[1]
        for numeral in self.numerals:
            # Adjust the `top_left` and `bottom_right` members of the numerals.
            numeral.top_left = (numeral.top_left[0] - extra_offset[0],
                                numeral.top_left[1] - extra_offset[1])
            numeral.bottom_right = (numeral.bottom_right[0] - extra_offset[0],
                                    numeral.bottom_right[1] - extra_offset[1])

    def reconfigure(self) -> bool:
        """Amend the position and orientation of the tile based on the positions of its
        safely identified (good) numerals.
        """
        good_numerals = [(i, numeral) for (i, numeral) in enumerate(self.numerals)
                                      if numeral.good()]
        if not good_numerals:
            return False  # This tile is lost.

        if len(good_numerals) == 1:
            return True
            # Compare the good numeral's actual position to the ROI center.
            # good_numeral = good_numerals[0][1]
            # extra_offset = tuple(map(round, good_numeral.get_offset()))
            numeral_index = good_numerals[0][0]
            extra_offset = tuple(map(round, self.get_numeral_roi_offset(numeral_index)))
            # self.add_offset(extra_offset)
            self.offset = (self.offset[0] + extra_offset[0],
                           self.offset[1] + extra_offset[1])
            for numeral in self.numerals:
                # Reduce the ROI size of the bad numerals on this tile since we have a
                # better idea of where they should be now.  XXX: this invalidates the
                # saved position, but we will relocate the numeral anyway.
                if numeral.bad():
                    numeral.roi_height = 130
                    numeral.roi_width  = 110

        # elif True:
        #     return True

        # XXX: We don't really need to support running this with an already rotated tile,
        # because we would already have used the same two numerals (which we didn't
        # reidentify) the last time we got here.  It would be kind of nice if it doesn't
        # break things, though.
        elif len(good_numerals) == 2:
            # There are two.  We can determine an angle and reducing the ROI size more
            # aggressively should be safe.  FIXME: increase the ROI dimenstions based on
            # the angle?
            i, j = good_numerals[0][0], good_numerals[1][0]
            old_expected_offsets = (
                self.get_expected_offset(0),
                self.get_expected_offset(1),
                self.get_expected_offset(2))
            self.angle = self.determine_angle(i, j)

            # Compute expected offsets for the two good numerals based on the tile's
            # rotation.  FIXME: this currently computes the same rotation matrix twice,
            # which is kind of stupid.
            expected_offsets = self.get_expected_offset(i), self.get_expected_offset(j)

            # Get the numeral's actual offsets.
            actual_offsets = self.get_numeral_offset(i), self.get_numeral_offset(j)

            # Compare the actual offsets to the expected ones.  I.e., check how much the
            # actual offsets differ from the ones we expect after adjusting for rotation.
            # The mean of these differences will become the tile's new offset.
            offset_differences = (
                (actual_offsets[0][0] - expected_offsets[0][0],
                 actual_offsets[0][1] - expected_offsets[0][1]),
                (actual_offsets[1][0] - expected_offsets[1][0],
                 actual_offsets[1][1] - expected_offsets[1][1]))

            # Compute the coordinate-wise mean of the offset differences.  (The first
            # element is the mean of the rows, the second the mean of the columns.)
            offset_correction = (
                (offset_differences[0][0] + offset_differences[1][0]) / 2,
                (offset_differences[0][1] + offset_differences[1][1]) / 2)
            old_offset = self.offset
            # This is our best guess for the tile's offset.  FIXME: don't round yet?
            # self.offset = (int(round(offset_correction[0])),
            #                int(round(offset_correction[1])))
            # FIXME.
            self.offset = (int(round(self.offset[0] + offset_correction[0])),
                           int(round(self.offset[1] + offset_correction[1])))

            for numeral_index, numeral in enumerate(self.numerals):
                # Adjust the `top_left` and `bottom_right` members of the numerals to be
                # relative to the new ROI top-left.  XXX: just using `self.offset` is not
                # enough here, since the numeral's ROI location also depends on the angle.
                # FIXME: this code is only correct when there was no offset and the angle
                # was zero before we changed it, I think.  TODO: store were numerals were
                # found relative to the tile center?  TODO: don't round yet?
                # old_expected_offset = self.default_numeral_offsets[numeral_index]
                old_expected_offset = (
                    old_expected_offsets[numeral_index][0] + old_offset[0],
                    old_expected_offsets[numeral_index][1] + old_offset[1])
                new_expected_offset = self.get_expected_offset(numeral_index)
                # Add the tile's offset.
                new_expected_offset = (
                    new_expected_offset[0] + self.offset[0],
                    new_expected_offset[1] + self.offset[1])
                numeral_offset_correction = (
                    old_expected_offset[0] - new_expected_offset[0],
                    old_expected_offset[1] - new_expected_offset[1])
                # numeral.top_left = (
                #     int(round(numeral.top_left[0] + numeral_offset_correction[0])),
                #     int(round(numeral.top_left[1] + numeral_offset_correction[1])))
                # numeral.bottom_right = (
                #     int(round(numeral.bottom_right[0] + numeral_offset_correction[0])),
                #     int(round(numeral.bottom_right[1] + numeral_offset_correction[1])))
                if numeral.bad():
                    numeral.roi_height = 115
                    numeral.roi_width  = 95
                    # # FIXME: adjust the ROI dimenstions for the angle the right way.
                    # if numpy.degrees(self.angle) < 3:
                    #     numeral.roi_height = 115
                    #     numeral.roi_width  = 95
                    # else:
                    #     numeral.roi_height = 120
                    #     numeral.roi_width  = 100
        else:
            raise AssertionError

        return True

    # The height of one hexagonal tile in the warped, cropped, and resized image of the
    # game board.  We know this from the image we use to locate and crop the board with
    # SURF.  The individual tiles were scanned at the same resolution.  (This accounts for
    # some gaps between the tiles; the real height is closer to 372.)
    height: int = 378

    # XXX: The first dimension corresponds to rows, the second to columns.  I.e., these
    # are (num_rows, num_columns) and not (x, y) pairs.  The origin is at the top-left.
    default_numeral_offsets = (-120, -8), (64, -111), (64, 111)


def _get_default_tile_centers() -> Tuple[Tuple[int, int]]:
    centers = []
    # Start with the tile at the top-left and add the coordinates column by column.  I.e.,
    # the outer loop moves one column to the right, and the inner loop moves one tile
    # down.
    image_height = _board_template.shape[0]
    image_width  = _board_template.shape[1]
    x_offset = Tile.height * numpy.cos(2 * numpy.pi / 12)
    for col in -2, -1, 0, 1, 2:
        x = image_width / 2 + col * x_offset
        num_rows = 5 - abs(col)
        y = image_height / 2 - (num_rows - 1) / 2 * Tile.height
        for row in range(num_rows):
            centers.append((int(round(y)), int(round(x))))
            y += Tile.height
    return tuple(centers)

class Board():
    """Representation of one player's filled game board."""

    def __init__(self, board_image: numpy.ndarray):
        """The image should be warped, cropped, and resized to only show the filled game
        board.
        """
        self.board_image = board_image
        # XXX: At first I did `self.tiles = (Tile(),) * 19`.  That results in a tuple with
        # the same object 19 times (`self.tiles[0] is self.tiles[1]` would be `True`).
        # See [1] (note 2) and [2].
        self.tiles = tuple(Tile() for _ in range(19))
        self.rois_used = tuple([None] * 3 for _ in range(19))
    # [1]: https://docs.python.org/3/library/stdtypes.html#common-sequence-operations
    # [2]: https://docs.python.org/3/faq/programming.html#faq-multidimensional-list

    def get_tile_center(self, tile_index: int) -> Tuple[int, int]:
        default_center = self._default_tile_centers[tile_index]
        offset = self.tiles[tile_index].offset
        return default_center[0] + offset[0], default_center[1] + offset[1]

    def get_numeral_roi(self, tile_index: int, numeral_index: int):
        tile           = self.tiles[tile_index]
        tile_center    = self.get_tile_center(tile_index)
        numeral_offset = tile.get_expected_offset(numeral_index)

        roi_center = tile_center[0] + numeral_offset[0], \
                     tile_center[1] + numeral_offset[1]

        numeral = tile.numerals[numeral_index]
        width   = numeral.roi_width
        height  = numeral.roi_height

        # XXX: Somehow explicitly converting to `int` is necessary.  I think it has to do
        # with the arguments of `round()` being of type `numpy.float64`.
        top    = int(round(roi_center[0] - height / 2))
        bottom = int(round(roi_center[0] + height / 2))
        left   = int(round(roi_center[1] - width  / 2))
        right  = int(round(roi_center[1] + width  / 2))

        return top, bottom, left, right

    def get_image_slice(self, tile_index: int, numeral_index: int):
        top, bottom, left, right = self.get_numeral_roi(tile_index, numeral_index)
        # Store what ROI was used for identifying the numeral.
        self.rois_used[tile_index][numeral_index] = top, bottom, left, right
        return self.board_image[top:bottom, left:right]

    def identify_numeral(self, tile_index: int, numeral_index: int):
        """Identify what digit is shown in the ROI of a Numeral object.  It should be
        known that exactly one digit is really there.
        """
        numeral = self.tiles[tile_index].numerals[numeral_index]
        image_slice = self.get_image_slice(tile_index, numeral_index)
        candidates = Numeral.candidate_triples[numeral_index]

        digit = candidates[0]
        angle = self.tiles[tile_index].angle
        score, top_left, bottom_right = match_digit(image_slice, digit, angle)
        confidence = 1e9
        for candidate in candidates[1:]:
            results = match_digit(image_slice, candidate, angle)
            if results[0] < score:
                # If the new score is half of the previously best one, we assign a
                # confidence of 2.  This doesn't have any real statistical meaning.  TODO:
                # rename?
                confidence = score / results[0]
                digit = candidate
                score, top_left, bottom_right = results
            elif results[0] / score < confidence:
                # Lower our confidence.
                confidence = results[0] / score
        assert(confidence >= 1)

        numeral.digit = digit
        numeral.confidence = confidence
        numeral.top_left = top_left[::-1]  # FIXME...
        numeral.bottom_right = bottom_right[::-1]  # FIXME...

        # TODO: Adjust `numeral.top_left` and `numeral.bottom_right` to be relative to the
        # default tile center?  Or maybe just relative to the tile's actual center.
        # FIXME: DRY.
        # tile           = self.tiles[tile_index]
        # tile_center    = self.get_tile_center(tile_index)
        # numeral_offset = tile.get_expected_offset(numeral_index)
        # roi_center = tile_center[0] + numeral_offset[0], \
        #              tile_center[1] + numeral_offset[1]
        roi_top, _, roi_left, _ = self.get_numeral_roi(tile_index, numeral_index)
        # It's relative to the ROI top-left right now.  Add the distance from the ROI
        # top-left to the default tile center...

        tile_center = self._default_tile_centers[tile_index]
        # Make the coordinates absolute.
        numeral.top_left = (numeral.top_left[0] + roi_top,
                            numeral.top_left[1] + roi_left)
        numeral.top_left = (numeral.top_left[0] - tile_center[0],
                            numeral.top_left[1] - tile_center[1])
        numeral.bottom_right = (numeral.bottom_right[0] + roi_top,
                                numeral.bottom_right[1] + roi_left)
        numeral.bottom_right = (numeral.bottom_right[0] - tile_center[0],
                                numeral.bottom_right[1] - tile_center[1])

    def identify_all_numerals(self, noisy=False):
        for i, tile in enumerate(self.tiles):
            if noisy: print(f'Tile {i + 1:>2}: ', end='')
            for j, numeral in enumerate(tile.numerals):
                self.identify_numeral(tile_index=i, numeral_index=j)
                if noisy: print(f'{numeral.digit} ({numeral.confidence:.3f}); ', end='')
            if noisy: print()

        # Amend the configuration of tiles as long as this lifts any numerals above the
        # bad threshold.
        i = 0
        while True:
            bad_numerals = self._get_bad_numerals()
            if not bad_numerals:
                break
            bad_tiles = set(tile_index for tile_index, _ in bad_numerals)
            if noisy:
                print(f'{len(bad_numerals)} numerals in {len(bad_tiles)} '
                      'tiles were identified with poor confidence.')

            # Only for debugging.  FIXME: remove.
            if noisy:
                self.show(f'Unfinished board {i} ({id(self)})')

            # I will try to fix you.
            for tile_index in bad_tiles:
                tile = self.tiles[tile_index]
                # Reconfigure these tiles.  I.e., amend their position and orientation.
                # TODO: only reconfigure tiles with amended numerals.
                tile.reconfigure()
                # Try to identify the tile's bad numerals again.
                for numeral_index in 0, 1, 2:
                    numeral = tile.numerals[numeral_index]
                    if numeral.good():
                        continue
                    if noisy:
                        print(f'Tile {tile_index + 1:>2}: {numeral.digit} '
                              f'({numeral.confidence:.3f}) ', end='')
                    self.identify_numeral(tile_index, numeral_index)
                    if noisy:
                        print(f'-> {numeral.digit} ({numeral.confidence:.3f})')

            # Check which numerals are still bad and which are good now.
            cursed_numerals  = self._get_bad_numerals()
            amended_numerals = list(set(bad_numerals) - set(cursed_numerals))

            if not amended_numerals:
                break  # We can't save any more numerals here.
            i += 1

        # TODO: do something clever with color.

        # TODO: sanity-check the results; does any tile appear more than once?

    def _get_bad_numerals(self) -> List[Tuple[int, int]]:
        return [(i, j) for i, tile in enumerate(self.tiles)
                       for j, numeral in enumerate(tile.numerals)
                       if numeral.bad()]

    def score():
        """Compute the score of a fully identified board."""
        pass

    def get_numeral_location(self, tile_index: int, numeral_index: int):
        tile    = self.tiles[tile_index]
        numeral = tile.numerals[numeral_index]

        origin = self._default_tile_centers[tile_index]

        left   = origin[1] + numeral.top_left[1]
        right  = origin[1] + numeral.bottom_right[1]
        top    = origin[0] + numeral.top_left[0]
        bottom = origin[0] + numeral.bottom_right[0]

        return top, bottom, left, right

    def draw_tile_center(self, tile_index, image):
        default_center = self._default_tile_centers[tile_index]
        cv2.circle(image, center=default_center[::-1], radius=10, color=(255, 255, 255),
                   thickness=-1)
        was_amended = self.tiles[tile_index].offset != (0, 0)
        if was_amended:
            center = self.get_tile_center(tile_index)
            cv2.circle(image, center=center[::-1], radius=10, color=(0, 0, 255),
                       thickness=-1)
        else:
            center = default_center
        # Draw a line to visualize the tile's detected angle.
        angle: float = self.tiles[tile_index].angle
        if angle != 0:
            center = numpy.array(center)
            # FIXME: DRY.
            sin = numpy.sin(angle)
            cos = numpy.cos(angle)
            rotation = numpy.matrix([[cos, -sin],
                                    [sin,  cos]])
            offsets = numpy.array([[0], [200]]), numpy.array([[0], [-200]])
            points = (
                tuple(numpy.round(center + (rotation * offsets[0]).A1).astype(int)),
                tuple(numpy.round(center + (rotation * offsets[1]).A1).astype(int)))
            cv2.line(image, points[0][::-1], points[1][::-1], color=(255, 255, 255),
                     thickness=2, lineType=cv2.LINE_AA)

    # TODO: support passing an image on which everything will be drawn (e.g., the BGR
    # image of the board).
    def show(self, name: str, draw_digits=True, draw_rois=True, draw_tile_centers=True,
             draw_lines=True):
        # Prepare the image for displaying: make it less unpleasant to look at.
        labeled = self.board_image.copy()
        mean_intensity = labeled.mean()
        if mean_intensity > 48:
            # Darken the image so we can draw bright stuff on it and have it be legible.
            labeled = (48 / mean_intensity * labeled.astype(numpy.float32)).astype(
                numpy.uint8)
        labeled = cv2.cvtColor(labeled, cv2.COLOR_GRAY2BGR)

        for tile_index, tile in enumerate(self.tiles):
            self.draw_tile_center(tile_index, labeled)
            for numeral_index, numeral in enumerate(tile.numerals):
                top, bottom, left, right = self.rois_used[tile_index][numeral_index]
                cv2.rectangle(labeled, (left, top), (right, bottom),
                              color=(160, 160, 160), thickness=2)
                top, bottom, left, right = self.get_numeral_location(tile_index,
                                                                     numeral_index)
                cv2.rectangle(labeled, (left, top), (right, bottom), color=(64, 64, 255),
                              thickness=2)
                # This is where the bottom-left corner of the number will be drawn.
                number_pos = right + 2, bottom - 10
                cv2.putText(labeled, str(numeral.digit), org=number_pos,
                            fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=3.3,
                            color=(64, 64, 255), thickness=5, lineType=cv2.LINE_AA)

        cv2.namedWindow(name, cv2.WINDOW_NORMAL)
        cv2.imshow(name, labeled)

    # Approximate center points of where the 19 tiles should be in the warped and resized
    # photo of the game board.  Used to define ROIs for template matching the digits.
    _default_tile_centers = _get_default_tile_centers()


def match_digit(image, n, angle=.0):
    """Find the number n in an image using template matching."""
    template, mask = get_digit_template(n, angle)
    result = cv2.matchTemplate(image, template, method=cv2.TM_SQDIFF, mask=mask)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    # Normalize the score: divide by the number of pixels in the template image so we
    # don't have a bias towards small images.  Without this, small images would naturally
    # have lower (better) scores than bigger ones.  For example, my image for the digit 1
    # is smaller than those for 5 and 9 and therefore yields smaller squared difference
    # values on average.
    score = min_val / template.size
    top_left = min_loc
    bottom_right = top_left[0] + template.shape[1], top_left[1] + template.shape[0]
    return score, top_left, bottom_right
# [1]: https://docs.opencv.org/master/df/dfb/group__imgproc__object.html#ga586ebfb0a7fb604b35a23d85391329be
# [2]: https://docs.opencv.org/master/de/da9/tutorial_template_matching.html
# [3]: https://docs.opencv.org/master/d4/dc6/tutorial_py_template_matching.html


# This was much more straightforward than I expected.  I also really thought I'd have to
# take the maximum.
def pixelwise_minimum(image):
    """Construct a grayscale image from a color image by taking the minimum of all
    channels at each pixel."""
    return image.min(axis=2)  # Call `numpy.ndarray.min` [1].
# [1]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.min.html


def color_to_noise(image):
    """Construct a grayscale image from a BGR image.  Pixels that already mostly are a
    shade of gray barely change.  Pixels that are pretty colorful are changed to random
    noise.

    An image of random noise in the range [-255, 255] is generated and multiplied by the
    squares of the input images' saturation and value channels (each is divided by 255
    first).  This noise is added to the pixelwise minimum of the input image.  The pixel
    values of the resulting image are clipped to [0, 255] before that image is returned.
    """
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
    morphed_image = image

    hsv_image = cv2.cvtColor(morphed_image, cv2.COLOR_BGR2HSV)
    noise = 510. * numpy.random.random(hsv_image.shape[:2]) - 255.
    saturation = hsv_image[:, :, 1].astype(numpy.float32)
    saturation = cv2.erode(saturation, kernel, iterations=1)
    saturation *= 1. / 255.
    value = hsv_image[:, :, 2].astype(numpy.float32)
    value = cv2.erode(value, kernel, iterations=1)
    value *= 1. / 255.
    noise *= saturation * saturation * value * value

    # Taking the pixelwise maximum is better than the minimum now (when combined with
    # adaptive thresholding).  It's also seems to be very marginally better than just
    # converting to grayscale the normal way (`cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)`).
    min_image = image.max(axis=2)  # FIXME: rename this?!

    # Whoa.  This kills it.  See [1].  Adaptive Gaussian thresholding gives very similar
    # results.
    min_image = cv2.adaptiveThreshold(min_image, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                      cv2.THRESH_BINARY, blockSize=15, C=2)
    # [1]: https://docs.opencv.org/master/d7/d4d/tutorial_py_thresholding.html

    # The erosion helps a little.  An opening with the same kernel is basically neutral.
    # (Update after adding adaptive thresholding: the erosion still helps a little and is
    # still better than the opening.)
    disk = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    min_image = cv2.erode(min_image, disk)

    # TODO: does this still make any difference?
    noisy_image = min_image.astype(numpy.float32) + noise
    noisy_image = noisy_image.clip(min=0, max=255)
    return noisy_image.astype(numpy.uint8)
# [1]: https://en.wikipedia.org/wiki/HSL_and_HSV#Hue_and_chroma
# [2]: http://scikit-image.org/docs/dev/api/skimage.exposure.html#skimage.exposure.rescale_intensity


def test_lots_of_preprocessing(photo):
    # Convert to HSV.  See what the 3 channels (hue, saturation, and value) actually look
    # like.
    hsv_board = cv2.cvtColor(photo, cv2.COLOR_BGR2HSV)

    # See <https://docs.opencv.org/master/d5/d0f/tutorial_py_gradients.html>.
    laplacian = cv2.Laplacian(photo, cv2.CV_64F)  # This one kind of highlights the
                                                  # digits.
    gray_board = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
    sobel_x = cv2.Sobel(gray_board, cv2.CV_64F, 1, 0, ksize=-1)
    sobel_y = cv2.Sobel(gray_board, cv2.CV_64F, 0, 1, ksize=-1)

    denoised_board = hsv_board.copy()
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    denoised_board[:, :, 2] = cv2.morphologyEx(hsv_board[:, :, 2], cv2.MORPH_OPEN, kernel)
    denoised_board[:, :, 1] = cv2.GaussianBlur(hsv_board[:, :, 1], ksize=(19, 19), sigmaX=0)
    denoised_board = cv2.cvtColor(denoised_board, cv2.COLOR_HSV2BGR)

    # Find the 12 most dominant colors: two of them should be black and white, one should
    # be the background color, and the remaining 9 colors should be the actual lines.
    # quantized_board = quantize_colors(denoised_board)

    edges_1 = cv2.Canny(denoised_board, 50, 150)
    # edges_2 = cv2.Canny(denoised_board, 900, 1000, apertureSize=5)

    cv2.namedWindow('Denoised board', cv2.WINDOW_NORMAL)
    cv2.imshow('Denoised board', denoised_board)

    cv2.namedWindow('Edges 1', cv2.WINDOW_NORMAL)
    cv2.imshow('Edges 1', edges_1)

    cv2.namedWindow('Hue', cv2.WINDOW_NORMAL)
    cv2.imshow('Hue', cv2.extractChannel(hsv_board, 0))
    cv2.namedWindow('Saturation', cv2.WINDOW_NORMAL)
    cv2.imshow('Saturation', cv2.extractChannel(hsv_board, 1))
    cv2.namedWindow('Value', cv2.WINDOW_NORMAL)
    cv2.imshow('Value', cv2.extractChannel(hsv_board, 2))

    cv2.namedWindow('Gradient', cv2.WINDOW_NORMAL)
    cv2.imshow('Gradient', laplacian)
    cv2.namedWindow('Sobel X', cv2.WINDOW_NORMAL)
    cv2.imshow('Sobel X', sobel_x)
    cv2.namedWindow('Sobel Y', cv2.WINDOW_NORMAL)
    cv2.imshow('Sobel Y', sobel_y)


# FIXME: the error when the photo doesn't exist is pretty cryptic.
def main():
    # `argparse` (https://docs.python.org/3/library/argparse.html) would be overkill at
    # this point.
    if len(sys.argv) < 2:
        print(f'{sys.argv[0]}: file path required', file=sys.stderr)
        exit(2)
        return
    else:
        photo_path = sys.argv[1]

    board = locate_board(photo_path)  # This works quite well.

    # TODO.  Maybe divide the board into hexagons.  I'm reluctant to just divide into
    # hexagons based on knowledge of the board as players wouldn't usually place their
    # tiles too precisely.  Maybe some sort of edge detection or a Generalized Hough
    # Transform could help.

    cv2.namedWindow('Board', cv2.WINDOW_NORMAL)
    cv2.imshow('Board', board)

    pixelwise_minimum_board = pixelwise_minimum(board)
    cv2.namedWindow('Pixelwise minimum', cv2.WINDOW_NORMAL)
    cv2.imshow('Pixelwise minimum', pixelwise_minimum_board)

    noisy_board = color_to_noise(board)
    cv2.namedWindow('Noisy board', cv2.WINDOW_NORMAL)
    cv2.imshow('Noisy board', noisy_board)

    game_board = Board(pixelwise_minimum_board)
    game_board.identify_all_numerals(noisy=True)
    game_board.show('Labeled minimum board')

    game_board = Board(noisy_board)
    game_board.identify_all_numerals(noisy=True)
    game_board.show('Labeled noisy board')

    # game_board = Board(gray_board)
    # gray_board.identify_all_numerals(noisy=True)

    # Wait until all windows were closed.
    while cv2.waitKey() != -1:
        pass


if __name__ == '__main__':
    main()
