"""Compare the data in a set of CSV files created by `score_all.py` to the results from a
baseline version of the CV algorithm.  Print information about any regressions and a
summary.
"""

import argparse
import bisect  # Gives us a binary search for sorted lists.
import csv
import math
import os


# TODO: return an object of this class from `compare()`.
class Summary:
    pass


def read_csv_file(path):
    data = []
    with open(path, newline='') as csv_file:
        reader = csv.DictReader(csv_file, strict=True)
        for row in reader:
            # TODO: convert more fields?
            for key in 'top confidence', 'left confidence', 'right confidence':
                row[key] = float(row[key])
            # del row['tile']
            data.append(row)
    return tuple(data)


def read_csv_files(path):
    data = {}
    with os.scandir(path) as it:
        for entry in it:
            if not entry.is_file():
                continue
            data[os.path.splitext(entry.name)[0]] = read_csv_file(entry)
    return data


# See [1] and [2].  TODO: Implement some form of linear interpolation based on the values
# of `math.floor(i)` and `math.ceil(i)`?  See [3].
def percentile(sorted_list, percent):
    i = len(sorted_list) * percent
    return sorted_list[math.ceil(i)]
# [1]: https://en.wikipedia.org/wiki/Percentile#The_nearest-rank_method
# [2]: https://stackoverflow.com/q/2374640
# [3]: https://en.wikipedia.org/wiki/Percentile#The_linear_interpolation_between_closest_ranks_method


def get_confidence_list(performance_data):
    confidence_list = [tile_data[key]
        for board_data in performance_data.values()
        for tile_data in board_data
        for key in ('top confidence', 'left confidence', 'right confidence')]
    confidence_list.sort()
    return confidence_list


def compare(baseline_data, new_data):
    """Compare the data produced by two different versions of the CV algorithm for the
    same photo.
    """
    num_errors = 0
    num_errors_baseline = 0
    confidence_sum = 0
    confidence_sum_baseline = 0
    max_misid_confidences = [0, 0]
    regressions = []

    for baseline_row, row in zip(baseline_data, new_data):
        for key in 'top confidence', 'left confidence', 'right confidence':
            confidence_sum += row[key]
            confidence_sum_baseline += baseline_row[key]
        error_info = row['number of misidentified digits']
        error_info_baseline = baseline_row['number of misidentified digits']
        if not error_info and not error_info_baseline:  # Both are the empty string.
            continue
        if error_info:
            num_errors += int(error_info)
            tile_number = row['tile']
        if error_info_baseline:
            num_errors_baseline += int(error_info_baseline)
        for key in 'top', 'left', 'right':
            digit_string = row[key + ' digit']
            was_misidentified = len(digit_string) > 1
            was_misidentified_before = len(baseline_row[key + ' digit']) > 1
            if was_misidentified_before:
                # This digit was misidentified in the baseline data.
                confidence = baseline_row[key + ' confidence']
                max_misid_confidences[1] = max(max_misid_confidences[1], confidence)
            if not was_misidentified:
                continue
            confidence = row[key + ' confidence']
            max_misid_confidences[0] = max(max_misid_confidences[0], confidence)
            if was_misidentified_before:
                continue  # This digit was already misidentified before.
            # The digit wasn't misidentified before.  It's a regression.
            digit = int(digit_string[0])
            correct_digit = int(digit_string[3])
            baseline_confidence = baseline_row[key + ' confidence']
            regressions.append(f'Tile {tile_number:>2}: {key:>5} misid\'ed as '
                               f'{digit} (expected {correct_digit}), '
                               f'conf.: {confidence:.3f} (was {baseline_confidence:.3f})')

    mean_confidence = confidence_sum / 57
    mean_confidence_baseline = confidence_sum_baseline / 57

    return num_errors, num_errors_baseline, mean_confidence, mean_confidence_baseline, \
           max_misid_confidences, regressions


def compare_all(baseline_results, new_results):
    """Compare the results produced by one version of the algorithm to those produced by
    another for all photos.
    """
    num_photos_with_errors = 0
    num_photos_with_errors_baseline = 0
    total_num_errors = 0
    total_num_errors_baseline = 0
    mean_confidence_sum = 0
    mean_confidence_sum_baseline = 0
    max_misid_confidences = [0, 0]
    num_regressions = 0
    all_regressions = {}

    # Delete all items from `baseline_results` that don't have a corresponding item in
    # `new_results` and vice versa.  Only consider photos for which we have results in
    # both `dict`s.  See <https://docs.python.org/3/library/stdtypes.html#dict-views>.
    for key in baseline_results.keys() - new_results.keys():
        del baseline_results[key]
    for key in new_results.keys() - baseline_results.keys():
        del new_results[key]

    for key in sorted(new_results):
        num_errors, num_errors_baseline, mean_confidence, mean_confidence_baseline, \
            max_misid_confs, regressions = \
            compare(baseline_results[key], new_results[key])
        if num_errors != 0:
            num_photos_with_errors += 1
        if num_errors_baseline != 0:
            num_photos_with_errors_baseline += 1
        total_num_errors             += num_errors
        total_num_errors_baseline    += num_errors_baseline
        mean_confidence_sum          += mean_confidence
        mean_confidence_sum_baseline += mean_confidence_baseline
        if regressions != []:
            all_regressions[key] = regressions

        max_misid_confidences[0] = max(max_misid_confidences[0], max_misid_confs[0])
        max_misid_confidences[1] = max(max_misid_confidences[1], max_misid_confs[1])

    for filename, regressions in all_regressions.items():
        num_regressions += len(regressions)
        print(f'{filename}:')
        for message in regressions:
            print(' ', message)
    if all_regressions:
        num_photos_with_regressions = len(all_regressions)
        s = num_regressions != 1 and 's' or '', \
            num_photos_with_regressions != 1 and 's' or ''
        print(f'=> {num_regressions} identification regression{s[0]} in '
              f'{num_photos_with_regressions} photo{s[1]}\n')

    mean_confidence = mean_confidence_sum / len(new_results)
    mean_confidence_baseline = mean_confidence_sum_baseline / len(new_results)

    print(f'Photos with misidentifications: {num_photos_with_errors} '
          f'(baseline: {num_photos_with_errors_baseline}) out of {len(new_results)}')
    print(f'Misidentified digits:           {total_num_errors} '
          f'(baseline: {total_num_errors_baseline})')
    # print(f'Confidence regressions: TODO')
    print(f'Mean confidence:                {mean_confidence:.3f} '
          f'(baseline: {mean_confidence_baseline:.3f})')
    print(f'Highest-confidence misidentif.: {max_misid_confidences[0]:.3f} '
          f'(baseline: {max_misid_confidences[1]:.3f})')

    # See <https://en.wikipedia.org/wiki/Box_plot>.
    def print_box_plot_data(sorted_list, end='\n'):
        minimum        = sorted_list[0]
        first_centile  = percentile(sorted_list, 0.01)
        first_quartile = percentile(sorted_list, 0.25)
        median         = percentile(sorted_list, 0.5)
        third_quartile = percentile(sorted_list, 0.75)
        maximum        = sorted_list[-1]
        print(f'{minimum:.3f},       {first_centile:.3f},        {first_quartile:.3f},'
              f'  {median:.3f},        {third_quartile:.3f},   {maximum:.3f}', end=end)

    confidence_list = get_confidence_list(new_results)
    baseline_confidence_list = get_confidence_list(baseline_results)

    print('\n   Conf. minimum, 1st centile, 1st quartile, median, 3rd quartile, maximum:')
    print('           ', end='')
    print_box_plot_data(confidence_list)
    print('(baseline: ', end='')
    print_box_plot_data(baseline_confidence_list, end=')\n')

    def get_num_less(sorted_list, x):
        return bisect.bisect_left(sorted_list, x)

    print('\nNumber of identication margins strictly below...')
    for cutoff in 1.1, 1.2, 1.3:
        num_less = get_num_less(confidence_list, cutoff)
        num_less_baseline = get_num_less(baseline_confidence_list, cutoff)
        percent_less = 100 * num_less / (len(new_results) * 57)
        print(f'  * [{cutoff}]: {percent_less:.2f}% or {num_less} '
              f'(baseline: {num_less_baseline})')


def print_confidence_regressions(baseline_results, new_results):
    pass


def main():
    parser = argparse.ArgumentParser(description="""Compare the data in a set of CSV files
        created by `score_all.py` to the results from a baseline version of the CV
        algorithm.  Print information about any regressions and a summary.  When called
        without any arguments, the latest results are compared to the second latest.""")
    parser.add_argument('dir', nargs='?', help='directory with CSV files produced by '
                                               'score_all.py')
    parser.add_argument('-b', '--baseline', help='directory with CSV files that serve as '
                                                 'a baseline')
    args = parser.parse_args()

    new_results_path = args.dir
    baseline_results_path = args.baseline

    if new_results_path is None or baseline_results_path is None:
        basedir = os.path.join(os.path.dirname(__file__), '..', 'results')
        entries = sorted(os.listdir(basedir))
        if new_results_path is None:
            new_results_path = os.path.join(basedir, entries[-1])
            if baseline_results_path is None:
                baseline_results_path = os.path.join(basedir, entries[-2])
        elif baseline_results_path is None:
            baseline_results_path = os.path.join(basedir, entries[-1])

    assert (os.path.isdir(baseline_results_path))
    assert (os.path.isdir(new_results_path))

    baseline_results = read_csv_files(baseline_results_path)
    new_results      = read_csv_files(new_results_path)

    print(f"Comparing '{os.path.relpath(new_results_path)}' to "
          f"'{os.path.relpath(baseline_results_path)}' (baseline).\n")

    compare_all(baseline_results, new_results)


if __name__ == '__main__':
    main()
