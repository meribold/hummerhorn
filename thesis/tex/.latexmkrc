# See <https://tex.stackexchange.com/a/44316> and
# <https://tex.stackexchange.com/q/60204/78512>.

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  if ( $silent ) {
    # TODO: use the value of $_[0].
    system "makeglossaries -d '../.generated' -q thesis";
  }
  else {
    # TODO: ditto.
    system "makeglossaries -d '../.generated' thesis";
  };
}

push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
$clean_ext .= ' %R.ist %R.xdy';

# vim: ft=perl
