"""Score all test photos and record information about the algorithm's performance in CSV
files."""

import logging
import os
import subprocess
import sys

import cv2

import data
import score


def create_results_directory():
    """
    Create a directory for the CSV files and return the path name.  The directory's base
    name will look like this: 0026.d67a382.  In that example, d67a382 is the abbreviated
    hash of the latest commit and 26 is the total number of reachable commits from d67a382
    (ancestors of d67a382).  If the working tree has local modifications, "-dirty" is
    appended to the directory name (and the results shouldn't be commited).  See [1].

    [1]: https://softwareengineering.stackexchange.com/a/151558
         "How do you achieve a numeric versioning scheme with Git?"
    """
    stdout = subprocess.run(['git', 'rev-list', 'HEAD'],
                            stdout=subprocess.PIPE, check=True).stdout
    num_commits = stdout.count(b'\n')
    stdout = subprocess.run(['git', 'describe', '--always', '--long', '--dirty'],
                            stdout=subprocess.PIPE, check=True).stdout
    desc = stdout.decode().rstrip('\n')
    dir_path = os.path.join(os.path.dirname(__file__), '..', 'results',
                            f'{num_commits:04}.{desc}')
    dir_path = os.path.normpath(dir_path)
    try:
        os.makedirs(dir_path, exist_ok=False)
    except OSError:
        # If the directory already exists, remove it's contents.
        file_names = []
        for name in os.listdir(dir_path):
            extension = os.path.splitext(name)[1]
            if extension != '.csv':
                sys.exit(f'{dir_path} exists and contains an unexpected file: {name}')
            file_names.append(name)
        # Remove all the CSV files if no unexpected file was found.
        for name in file_names:
            file_path = os.path.join(dir_path, name)
            os.remove(file_path)
    return dir_path


def score_and_log(photo_filename, expected_tiles, results_dir):
    """Score one photo and record information about the algorithm's performance (number of
    misidentifications, confidence values of template matching) in a CSV file.
    """
    print(f'Scoring {photo_filename}... ', flush=True)
    photo_path = os.path.join(os.path.dirname(__file__), '..', 'photos', photo_filename)
    board_image = score.locate_board(photo_path)
    # board_image = cv2.cvtColor(board_image, cv2.COLOR_BGR2GRAY)
    # board_image = score.pixelwise_minimum(board_image)
    board_image = score.color_to_noise(board_image)
    board = score.Board(board_image)
    board.identify_all_numerals()
    tiles = board.tiles

    total_num_errors = 0
    confidence_sum = 0

    csv_path = os.path.join(results_dir, os.path.splitext(photo_filename)[0] + '.csv')
    with open(csv_path, mode='w') as f:
        f.write('tile,top digit,left digit,right digit,top confidence,left confidence,'
                'right confidence,number of misidentified digits\n')
        # Compare the tiles we identified to the ones that are actually on the photo and
        # record any misidentifications.
        for i, data in enumerate(zip(tiles, expected_tiles), start=1):
            f.write(f'{i}')
            tile, actual_digits = data
            digit_triple = tuple(numeral.digit for numeral in tile.numerals)
            confidence_triple = tuple(numeral.confidence for numeral in tile.numerals)
            num_errors = 0
            # TODO: Write a function?  Use some functional stuff?
            for found_digit, confidence, actual_digit in zip(
                    digit_triple, confidence_triple, actual_digits):
                confidence_sum += confidence
                if found_digit == actual_digit:
                    # \o/
                    f.write(f',{found_digit}')
                    continue
                # Q_Q
                num_errors += 1
                f.write(f',{found_digit} ({actual_digit})')
            total_num_errors += num_errors
            f.write(',{:.3f},{:.3f},{:.3f}'.format(*confidence_triple))
            f.write(num_errors > 0 and f',{num_errors}' or ',')
            f.write('\n')

    mean_confidence = confidence_sum / 57
    print(f'Results were written to {csv_path}')
    print(f'Misidentified digits: {total_num_errors}, mean confidence: '
          f'{mean_confidence:.3f}\n')
    return total_num_errors, mean_confidence


def main():
    total_num_errors = 0  # Godspeed...
    total_mean_confidence = 0
    results_dir = create_results_directory()
    photo_dir = os.path.join(os.path.dirname(__file__), '..', 'photos')
    num_photos = 0
    for dir_entry in sorted(os.scandir(photo_dir), key=lambda x: x.name):
        if not dir_entry.is_file():
            continue
        photo_filename = dir_entry.name
        key = int(photo_filename[:2])
        try:
            expected_tiles = data.photos[key]
        except KeyError:
            continue
        num_photos += 1
        num_errors, mean_confidence = score_and_log(photo_filename, expected_tiles,
                                                    results_dir)
        total_num_errors += num_errors
        total_mean_confidence += mean_confidence
    total_mean_confidence /= num_photos
    print(f'Total misidentified digits: {total_num_errors}, total mean confidence: '
          f'{total_mean_confidence:.3f}')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    # score.logger.setLevel(logging.DEBUG)
    main()
