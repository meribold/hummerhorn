# [Take it Easy!][1]

*Take it Easy!* is a board game that requires players to calculate their score after each
round.  The (WIP) Python (3.6) code [here](python/ "./python") aims to automate this using
computer vision.

![Photograph of one player's Take it Easy board after completing a round of the game](photos/06-2-blue-134-points.jpg "Figuring out how the score is calculated is left as an exercise to the reader.")

## Instructions

*   To analyze one photo (e.g.  [`11-green-140-points.jpg`]),
    run

    ```bash
    python3 python/score.py photos/11-green-140-points.jpg
    ```

    Be warned that this will open a gazillion windows.

[`11-green-140-points.jpg`]: photos/11-green-140-points.jpg

*   To analyze all photos, run

    ```bash
    python3 python/score_all.py
    ```

    A new directory with a bunch of CSV files will be created under
    [`./results`](./results).

*   To get a summary comparing the accuracy of two versions of the algorithm, run
    something like

    ```bash
    ./crunch --baseline results/0038.10148fa/ results/0043.2d98e01/
    ```

    When omitting both arguments, the CSV files in the latest subdirectory of
    [`./results`](./results) will be compared to the previous results.  See `./crunch -h`:

    ```
    usage: crunch [-h] [-b BASELINE] [dir]

    Compare the data from a baseline set of CSV files created by `score_all.py` to
    the data produced by a newer version of the CV algorithm. Print information
    about any regressions and a summary. When called without any arguments, the
    latest results are compared to the second latest.

    positional arguments:
      dir                   directory with CSV files produced by score_all.py

    optional arguments:
      -h, --help            show this help message and exit
      -b BASELINE, --baseline BASELINE
                            directory with CSV files that serve as a baseline
    ```

*   Given one of the directories with CSV files that `score_all.py` creates, we can check
    where exactly digits were misidentified with `grep`, `ack`, `ag`, `rg`, or whatever.
    For example:

    ```bash
    ag '[1-9] \([1-9]\)' results/0043.2d98e01/
    ```

*   Similarly, we can check which digits were identified with poor confidence:

    ```bash
    ag '1.0[0-9][0-9]' results/0043.2d98e01/
    ```

## Planned improvements to the CV algorithm

*   Reduce the ROI and rerun template matching for digits that were identified with low
    confidence.  A smaller ROI can be obtained using the positions of digits that were
    located with higher confidence.
*   Sanity-check results based on the game rules: no tile can appear more than once.
    Given that there are only 27 tiles of which 19 (randomly selected ones) will be used,
    there is a good chance that misidentifying a digit results in misidentifying its tile
    as one that was also found elsewhere.  Compare the confidence values in this case
    and run template matching again for the region with lower confidence using a smaller
    set of candidate digits.

### Done

*   Heavily penalize colored areas when trying to locate and identify digits with
    [template matching][2].  Maybe construct a grayscale image by taking the maximum of
    all channels at each pixel, and use that for template matching.
*   Consider using a [grayscale erosion][3] or [opening][4] operation to reduce some of
    the reflections (small bright spots) found in many of [my photos](photos/ "./photos").

## Other notes

*   Consider QR code scanners found on smartphones.  As far as I'm aware, they usually
    overlay a square on top of the camera feed indicating that the QR code has to be lined
    up with that square.  Given the maturity of QR code technology, this suggests such an
    approach is a good idea, and it translates pretty directly to Take it Easy.  A
    hypothetical app could display a hexagonal overlay, forcing the user to position their
    phone just right while taking a picture.  This might sidestep the issue of finding the
    board and fixing the perspective ([SURF][] already does a really good job, though).
    *   I checked and it doesn't look like QR code scanners really do that.  Did they use
        to?

[1]: https://en.wikipedia.org/wiki/Take_It_Easy_(game)
    "Take It Easy (game) - Wikipedia"
[2]: https://en.wikipedia.org/wiki/Template_matching
    "Template matching - Wikipedia"
[3]: https://en.wikipedia.org/wiki/Erosion_(morphology)#Grayscale_erosion
    "Erosion (morphology) - Wikipedia"
[4]: https://en.wikipedia.org/wiki/Opening_(morphology)
    "Opening (morphology) - Wikipedia"
[SURF]: https://en.wikipedia.org/wiki/Speeded_up_robust_features
    "Speeded up robust features - Wikipedia"
