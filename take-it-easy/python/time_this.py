from contextlib import contextmanager
from time import perf_counter as _timer

_time_stack = []


def _push_time():
    _time_stack.append(_timer())


def _pop_time(message, log):
    elapsed_time = _timer() - _time_stack.pop()
    if message is None:
        return
    depth = len(_time_stack)
    log('│ ' * depth + f'{message}: {elapsed_time:.3f} seconds')


# See [1].
@contextmanager
def time_this(message='', log=print):
    _push_time()
    # From [2]: "When an exception has occurred in the `try` clause and has not been
    # handled by an `except` clause [...], it is re-raised after the `finally` clause has
    # been executed."
    try:
        yield
    finally:
        _pop_time(message, log)
# [1]: https://docs.python.org/3/library/contextlib.html
# [2]: https://docs.python.org/3/tutorial/errors.html#defining-clean-up-actions
